cd `dirname $0`

docker rm -v koee_web -f

docker build -t web web
docker run --name koee_web -p 80:80 --restart=always -d web

docker images | awk '/<none/{print $3}' | xargs docker rmi
